(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.modal').modal();
    $('select').material_select();
    $('#modal1').modal('close');
    $('#modal11').modal('close');
    $('#ranking').modal('close');
    $('.datepicker').pickadate({
    selectMonths: true, // ß
    selectYears: 20 // 
  });

  }); // end of document ready
})(jQuery); // end of jQuery name space